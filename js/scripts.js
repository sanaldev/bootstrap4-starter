$(document).ready(function(){
    //commented tooltip script
    //$('[data-toggle="tooltip"]').tooltip();
    $("#mycarousel").carousel( { interval: 2000 } );
    //commented actions for the btn-group
    // $("#carousel-pause").click(function(){
    //     $("#mycarousel").carousel('pause');
    // });
    // $("#carousel-play").click(function(){
    //     $("#mycarousel").carousel('cycle');
    // });
    $("#carouselButton").click(function(){
        if($("#carouselButton").children("span").hasClass("fa-pause")) {
            $("#mycarousel").carousel('pause');
            $("#carouselButton").children("span").removeClass("fa-pause");
            $("#carouselButton").children("span").addClass("fa-play");
        } else if($("#carouselButton").children("span").hasClass("fa-play")){
            $("#mycarousel").carousel('cycle');
            $("#carouselButton").children("span").removeClass("fa-play");
            $("#carouselButton").children("span").addClass("fa-pause");
        }
    })
});
$('#loginBtn').click(function() {
    $('#loginModal').modal('show');
});
$('#reserveBtn').click(function() {
    $('#reserveFormModal').modal('show');
});
$('#reserveFormModal').on('shown.bs.modal', function () {
   $('#non-smoking').trigger('focus')
});
$('#loginModal').on('shown.bs.modal', function () {
    $('#exampleInputEmail3').trigger('focus')
});
//$("#mycarousel").on('slide.bs.carousel', ()=> { });
//$("#mycarousel").on('slid.bs.carousel', ()=> { })